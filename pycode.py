code = """
set b 93
set c b
jnz a 2
jnz 1 5
mul b 100
sub b -100000
set c b
sub c -17000
set f 1
set d 2
set e 2
set g d
mul g e
sub g b
jnz g 2
set f 0
sub e -1
set g e
sub g b
jnz g -8
sub d -1
set g d
sub g b
jnz g -13
jnz f 2
sub h -1
set g b
sub g c
jnz g 2
jnz 1 3
sub b -17
jnz 1 -23
"""
for line in code.split('\n'):
    args = line.split()[1:]
    if line.startswith("set") :
        print(f"{args[0]} = {args[1]}")
    if line.startswith("add") :
        print(f"{args[0]} += {args[1]}")
    if line.startswith("mul") :
        print(f"{args[0]} *= {args[1]}")
    if line.startswith("sub") :
        print(f"{args[0]} -= {args[1]}")
    if line.startswith("jnz") :
        print(f"if({args[0]} != 0) goto steps({args[1]})")

e=2
d=2
b = 93+100000
g=4
while g!=0:
    g=4
    g-=100093
    e+=1
    g=e
    g-=b
print(f"e:{e}")
print(f"d:{d}")
print(f"b:{b}")
print(f"g:{g}")
